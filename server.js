const express = require('express');
const got = require('got');
const config = require('config');

const app = express();

const key = config.get('weatherKey');

// Middleware
// app.use(express.json({ extended: false }));

// variables to store fetched weather data
let prevData = null;
let prevTime = null;

// GET request to server
// Not creating seperate folder/file as its only one api
app.get('/api', async (req, res) => {
  // res.send('Get is working'); // @debug

  // we can fetch new data only once in every 1.5 mins (1000 times a day)
  // https://openweathermap.org/price#weather (we r using onecall api)
  // Whenever new data is fetched, we save it to variable
  // whenever new req is made, we fetch new data only if
  // its been morethan 1.5 min since last data fetch
  if (prevTime == null || Date.now() - prevTime >= 90000) {
    try {
      const response = await got(
        `https://api.openweathermap.org/data/2.5/onecall?lat=-33.8688&lon=151.2093&exclude=current,minutely,hourly,alerts&appid=${key}&units=metric`,
      );
      prevTime = Date.now();
      prevData = response;
      res.json({ msg: 'Fetched Live', data: response.body });
    } catch (err) {
      console.log(err);
      res.status(500).send('Server Error');
    }
  } else {
    res.json({ msg: 'Fetched Stored', data: prevData.body });
  }
});

// Serve from static
app.use(express.static('client/build'));

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
