## WeatherMan Weather App

This app was created by kebinjoy@gmail.com for Ampion as part of the tech interview round. Please read the instructions below before you run the app.

*This app uses the OpenWeatherMap API to fetch live weather and display the number of days in Sydney where the temperature is predicted to be above 20 degrees in the next 5 days. It also shows how many days it is predicted to be sunny in the next 5 days. There is also a clock showing the locale time.*

---

## Setting up the environment

You'll need nodejs installed on your system to run this app.

1. Follow [these](https://nodejs.org/en/download/package-manager/) instructions to download and install nodejs on your system. While installation, make sure to check to add nodejs to the system PATH variables.
2. Once installed, make sure you have nodejs added to the path variables. If not added, you can follow [this](https://stackoverflow.com/questions/27864040/fixing-npm-path-in-windows-8-and-10) instruction on a windows machine.
3. Clone the project to an empty folder in your system. You can open a terminal in the target folder and enter **git clone https://bitbucket.org/KebinJoy/weatherman.git** to clone the repository. Remember, you need to have **git** installed on your system to do this.
   ![Cloning to folder Ampion](https://i.imgur.com/FHRRIhH.png)
4. Once cloned, open a terminal in the project folder. Make sure you are inside the project folder which will be called **weatherman** if you have cloned the project right. If you are outside the folder, change the folder using the **cd** command.
   ![Moving to weatherman folder](https://i.imgur.com/bcdSOVu.png)
5. Type **npm i** on the terminal and hit enter. It will install the required packages for the project.
   
   ![Installing Packages](https://i.imgur.com/5Ef4znE.png)
   
6. Once done, type **node server.js**
   
   ![Running server](https://i.imgur.com/OhVxdIh.png)
   
7. Open your browser and enter **http://localhost:5000/**
   ![Running app](https://i.imgur.com/c5n0Z7P.png)

---

## Documentation

This section explains the structure of the project and the external materials used.

**Project Structure**

1. The parent directory has server.js file which is the entry point to the application.
2. Server.js handles the incoming API requests and checks if the latest data fetched from OpenWeatherMap was before 1.5 mins ago. If yes, it fetches new data and sends it to the frontend. You can read the comments for a detailed explanation.
3. Other files in the parent directory are package.json, readme.md and gitignore.
4. Config folder contains config variable used for OpenWeatherMap API.
5. Client folder contains all the frontend of the app made using [react](https://reactjs.org/).
6. Inside the client folder, index.html in the public folder is the page rendered in the front end.
7. Src folder contains the components of the web page (for ex: footer, clock) and app.js which acts as a parent component for all other components.

Other unmentioned files include node_modules and package-lock.json. Build folder contains the final build app. You have it running on localhost:5000. To run the development server, you have to install packages inside the client folder too which can be done by entering **cd client** and **npm i**. Once installed, enter **cd ..** and **npm run dev**.

**Node Modules used**
1. [Config](https://www.npmjs.com/package/config) - To store configuration variables.
2. [Express](https://www.npmjs.com/package/express) - To run the server.
3. [Got](https://www.npmjs.com/package/got) - To communicate to external API.
4. [Axios](https://www.npmjs.com/package/axios) - To make requests from the frontend to the backend
5. [Material-ui](https://material-ui.com/) - To use material-ui components and icons

---

## OpenWeatherMap 

[OpenWeatherMap](https://openweathermap.org/) API allows registered user to use their public API free of cost. However, there's a limitation of the usage is in place which only allows the user to call the API a certain number of times a day.

In our app, we are using one call API, which will allow us to use the API **1000** times a day. Using the provided API key more than 1000 times a day will cause OpenWeatherMap to cease our API key. So to keep the API key unblocked, **we only fetch fresh data from OpenWeatherMap once in 1.5 minutes**. Doing so will ensure that our API key will not overflow the allowed usage limit. 

To achieve this, each time a new data is fetched from the OpenWeatherMap API, we save it at the backend with a timestamp. And when the backend receives a new request from the frontend asking for the fresh data, new data is fetched from the OpenWeatherMap server only if the previous request was at least 1.5 minutes ago. Or the backend responds frontend with the saved data.

On a live production server, the backend will be continuously running and there will always a copy of the previously fetched data. However during development, or when the server restarts quite many times, the backend memory will be cleared, resulting in the usage of the API key each time the server restarts. It can potentially overflow API usage limit but is highly unlikely. 

Another thing to be considered is when deploying the app on more than one machines. In such cases, the cumulative number of API calls from different machines can overflow the API usage limit. It can be avoided by using different API keys for different production server.

---

## Testing

As there is no user input other than mouse clicks, the main thing to test in the front end is the functionality of the website. Below are the screen shots of responsive test.
1. Web
   ![Web] (https://i.imgur.com/4TA0XFC.png)

2. Iphone X

   ![Iphone X] (https://i.imgur.com/RsEwN25.png)

3. Ipad Pro

   ![Ipad pro] (https://i.imgur.com/ytAY0vf.png)

On the backend, the following API testings were done.

1. [Geocoding API] (https://openweathermap.org/api/geocoding-api) - To find latitude and longitude of Sydney.
   ![Response] (https://i.imgur.com/dQajWVU.png)
2. [One Call API] (https://openweathermap.org/api/one-call-api) - To find weather for next 7 days in Sydney
   ![Response] (https://i.imgur.com/GkTs0AI.png)
3. Backend API - Its the API inside server.js which will respond with the stock data or newly fetched data.
   ![Response] (https://i.imgur.com/FQu2NFw.png)

## Thanks

The **Clock** component was created by [Yousef](https://medium.com/@yahm23) and the steps are available [here](https://medium.com/create-a-clocking-in-system-on-react/create-a-react-app-displaying-the-current-date-and-time-using-hooks-21d946971556).

Thanks to **Ampion and their team** for giving me the opportunity.

## Logical Error 

One call API returns 7 days forecast and I have used a for each loop for all the elements in that array instead looping through first 5 elements. Fixed Logical error on 19/04/21 11.30 am