import React, { useState, useEffect } from 'react';
import axios from 'axios';

// @material-ui function
import { makeStyles } from '@material-ui/core';

// @material-ui components
import Card from '@material-ui/core/Card';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';

// @material-ui icons
import WhatshotIcon from '@material-ui/icons/Whatshot';
import WbSunnyIcon from '@material-ui/icons/WbSunny';

import Clock from './Components/Clock';
import Footer from './Components/Footer';

const App = () => {
  // Stylng for material ui components
  const useStyles = makeStyles(() => ({
    card: {
      padding: '4vh',
      textAlign: 'center',
      minHeight: '30vh',
    },
    hotIcon: {
      color: '#ee1111',
      fontSize: '3em',
    },
    head: {
      fontFamily: 'sans-serif',
      fontSize: '2em',
    },
    sunIcon: {
      color: '#6ff',
      fontSize: '3em',
    },
  }));

  const classes = useStyles();

  // app state to conditional rendering
  const [weather, setWeather] = useState({ loading: true });

  // useEffect hook runs each time page reloads
  // We use it to fetch data from backend
  useEffect(() => {
    async function fetchWeather() {
      try {
        const weather = await axios.get('/api');
        console.log(weather.data.msg);

        let hotDays = 0;
        let sunnyDays = 0;

        // response from backend is string and thus we convert to JSON
        // We only need data for next 5 days
        for (let i = 0; i < 5; i++) {
          console.log(JSON.parse(weather.data.data).daily[i]);
          if (JSON.parse(weather.data.data).daily[i].temp.max > 20) {
            hotDays++;
          }
          if (
            JSON.parse(weather.data.data).daily[i].weather[0].main === 'Clear'
          ) {
            sunnyDays++;
          }
        }

        setWeather({ loading: false, hotDays, sunnyDays });
      } catch (err) {
        console.log(err); // @debug Show error in actual page?
      }
    }

    fetchWeather();
  }, []);
  return (
    <div>
      <Grid
        container
        spacing={3}
        alignContent={'center'}
        alignItems={'center'}
        justify={'center'}
      >
        <Grid item xs={12} className={classes.grid}>
          <Card className={classes.card} raised={true}>
            <Clock />
          </Card>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Card className={classes.card} raised={true}>
            <Grid container alignItems={'center'}>
              <Grid item xs={3}>
                <WhatshotIcon fontSize={'large'} className={classes.hotIcon} />
              </Grid>
              <Grid item xs={9} className={classes.head}>
                {weather.loading && <CircularProgress />}
                <div>
                  <h1>{weather.hotDays} </h1> days of hot weather with
                  temperature over 20° in the next 5 days.
                </div>
              </Grid>
            </Grid>
          </Card>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Card className={classes.card} raised={true}>
            <Grid
              container
              justify={'center'}
              alignContent={'center'}
              alignItems={'center'}
            >
              <Grid item xs={3}>
                <WbSunnyIcon fontSize={'large'} className={classes.sunIcon} />
              </Grid>
              <Grid item xs={9} className={classes.head}>
                {weather.loading && <CircularProgress />}
                <div>
                  <h1>{weather.sunnyDays} </h1>
                  <p>days of clear/sunny sky in the next 5 days</p>
                </div>
              </Grid>
            </Grid>
          </Card>
        </Grid>
      </Grid>

      <Footer />
    </div>
  );
};

export default App;
