// author : Yousef Ahmed
// https://medium.com/create-a-clocking-in-system-on-react/create-a-react-app-displaying-the-current-date-and-time-using-hooks-21d946971556

import React, { useEffect, useState } from 'react';

// @material-ui function
import { makeStyles } from '@material-ui/core';

const Clock = () => {
  const useStyles = makeStyles(() => ({
    clock: {
      fontFamily: 'sans-serif',
      fontSize: '2em',
    },
  }));

  const classes = useStyles();

  const [date, setDate] = useState(new Date());

  /**
   * Reassignes date variable with new value in every one second
   */
  useEffect(() => {
    const timer = setInterval(() => {
      setDate(new Date());
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  });

  return (
    <div className={classes.clock}>
      <h2>Sydney NSW, AU {date.toLocaleString()}</h2>
    </div>
  );
};

export default Clock;
