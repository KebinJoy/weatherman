import React from 'react';

// @material-ui function
import { makeStyles } from '@material-ui/core';

// @material-ui icons
import WarningIcon from '@material-ui/icons/Warning';

const Footer = () => {
  const useStyles = makeStyles(() => ({
    footer: {
      padding: 10,
      textAlign: 'center',
      bottom: '0',
      marginTop: '2vh',
      //   color: '#888888',
      backgroundColor: '#888888',
      width: '100%',
      fontFamily: 'sans-serif',
    },
    warningIcon: {
      color: '#f11',
    },
  }));

  const classes = useStyles();

  return (
    <footer className={classes.footer}>
      <WarningIcon className={classes.warningIcon} />
      This page is created as part of tech innterview round for{' '}
      <a href="https://www.ampion.com.au/">Ampion</a> by{' '}
      <a href="https://bitbucket.org/KebinJoy/">Kebin</a>. You can read more{' '}
      <a href="https://bitbucket.org/KebinJoy/weatherman/src/master/README.md">
        {' '}
        here
      </a>
      .
    </footer>
  );
};

export default Footer;
